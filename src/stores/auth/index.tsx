import React, { createContext, useState, useEffect } from "react";
import { app } from "../../config/app.firebase";

interface User {
  user: {
    id?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
  };
}

export const AuthContext = createContext<Partial<User>>({});

export const { Consumer, Provider } = AuthContext;

export const AuthProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<any>(null);

  useEffect(() => {
    app.auth().onAuthStateChanged(
      authUser => {
        localStorage.setItem("authUser", JSON.stringify(authUser));
        setUser(authUser);
      },
      () => {
        localStorage.removeItem("authUser");
        setUser(null);
      }
    );
  }, []);

  return <Provider value={{ user }}>{children}</Provider>;
};
