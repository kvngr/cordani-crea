import React from "react";
import { Router } from "@reach/router";

import { Home } from "./components/pages/Home";
import { Login } from "./components/pages/Login";
import { Planning } from "./components/pages/Planning";
import { PrivateRoute } from "./routes/PrivateRoute";
import { PublicRoute } from "./routes/PublicRoute";
import { AuthProvider } from "./stores/auth";

const App: React.FC = ({ children }) => {
  return (
    <AuthProvider>
      <Router>
        <PublicRoute exact path="/" component={Home} />
        <PublicRoute exact path="/login" component={Login} />
        <PrivateRoute exact path="/planning" component={Planning} />
      </Router>
    </AuthProvider>
  );
};

export default App;
