import firebase from "firebase/app";
import "firebase/auth";

export const app = firebase.initializeApp({
  apiKey: "AIzaSyDxdguOsrFcSRyAP6o1CJaX6_a0HTZjBPk",
  authDomain: "cordanicrea.firebaseapp.com",
  databaseURL: "https://cordanicrea.firebaseio.com",
  projectId: "cordanicrea",
  storageBucket: "cordanicrea.appspot.com",
  messagingSenderId: "407258273759",
  appId: "1:407258273759:web:380bce641667fbd670566a",
  measurementId: "G-DCHC893C6G"
});
