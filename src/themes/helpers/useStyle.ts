import { makeStyles, Theme } from "@material-ui/core/styles";
import grey from "@material-ui/core/colors/grey";

const rootColor = grey[50];
const appBarColor = grey[900];
const cardColor = grey[100];

export const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: rootColor,
    fontFamily: "Roboto"
    // height: "100%"
  },
  appBar: {
    backgroundColor: appBarColor
  },
  card: {
    display: "flex",
    maxWidth: 800,
    margin: "auto",
    backgroundColor: cardColor
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  listLinkNavbar: {
    flex: 1,
    listStyle: "none"
  },
  toolbar: {
    display: "flex"
  },
  wrapperModal: {
    textAlign: "center",
    color: theme.palette.text.secondary,
    backgroundColor: theme.palette.background.paper,
    borderRadius: "16px",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: "500px",
    maxHeight: "500px",
    width: "100%",
    height: "100%",
    margin: "auto"
  },
  formInputField: {
    margin: "16px 0 0 0"
  },
  formButton: {
    margin: "30px 0 0 0"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
}));
