import React, { ReactNode } from "react";
import { useStyles } from "../../themes/helpers/useStyle";

interface PageTemplateProps {
  children: ReactNode;
  header?: ReactNode;
  footer?: ReactNode;
}

export const PageTemplate = ({
  children,
  header,
  footer
}: PageTemplateProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {header && <>{header}</>}
      {children}
      {footer && <>{footer}</>}
    </div>
  );
};
