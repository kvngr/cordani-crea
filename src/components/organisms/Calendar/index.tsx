import React, { useRef, useState, ChangeEvent, SyntheticEvent } from "react";
import { useForm, Controller } from "react-hook-form";
import { TextField } from "@material-ui/core";
import FullCalendar from "@fullcalendar/react";
import { EventInput } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import { AbstractModal } from "../../molecules/Modal";
import { errorsList } from "../../../utils/errors";
import "./assets/style.scss";

interface CalendarState {
  calendarWeekends: boolean;
  calendarEvents: EventInput[];
  modalIsActive: boolean;
}

type FormData = {
  title: string;
  description: string;
};

export const Calendar = ({}) => {
  const calendarRef = useRef<FullCalendar>(null);

  const { register, unregister, errors, control } = useForm<FormData>({
    mode: "onChange"
  });

  const [state, setState] = useState<CalendarState>({
    calendarWeekends: true,
    calendarEvents: [],
    modalIsActive: false
  });

  // const handleOnCloseModal = () => {
  //   setState({ ...state, modalIsActive: !state.modalIsActive });
  // };

  // const handleOnClick = (args: any) => {
  //   // arg.dateStr
  //   setState({ ...state, modalIsActive: !state.modalIsActive });
  // };

  const handleToggleModal = () =>
    setState({ ...state, modalIsActive: !state.modalIsActive });

  // const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {

  //   if(event.target.value) {
  //     setState({
  //       ...state,
  //       calendarEvents: state.calendarEvents.concat({
  //         title: "Nouvel événement",
  //         start: arg.date,
  //         allDay: arg.allDay
  //       })
  //     });
  //   }

  // }

  return (
    <>
      <AbstractModal onToggle={handleToggleModal} open={state.modalIsActive}>
        <form>
          <Controller
            name="title"
            as={
              <TextField
                inputRef={register({
                  validate: value => value !== "" || errorsList.generic.unfilled
                })}
                error={!!errors.title}
                helperText={errors.title && errors.title.message}
              />
            }
            control={control}
            id="tandard-error-helper-text"
            label="Titre"
          />
          <Controller
            name="description"
            as={
              <TextField
                inputRef={register({
                  validate: value => value !== "" || errorsList.generic.unfilled
                })}
                error={!!errors.description}
                helperText={errors.description && errors.description.message}
              />
            }
            control={control}
            id="tandard-error-helper-text"
            label="Description"
          />
        </form>
      </AbstractModal>
      <div className="calendar-wrapper">
        <div className="app-calendar">
          <FullCalendar
            defaultView="timeGridWeek"
            header={{
              left: "prev,next today",
              center: "title",
              right: "timeGridWeek,timeGridDay,listWeek"
            }}
            plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
            ref={calendarRef}
            weekends={state.calendarWeekends}
            events={state.calendarEvents}
            dateClick={handleToggleModal}
          />
        </div>
      </div>
    </>
  );
};
