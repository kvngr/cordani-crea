import React from "react";
import { Navbar } from "../../molecules/Navbar";

interface HeaderProps {}

export const Header = ({}: HeaderProps) => {
  return (
    <>
      <Navbar />
    </>
  );
};
