import React from "react";
import { Grid, Card } from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import { PageTemplate } from "../../templates/pageTemplate";
import { Header } from "../../organisms/Header";
import { useStyles } from "../../../themes/helpers/useStyle";
import { Calendar } from "../../organisms/Calendar";

interface PlanningProps extends RouteComponentProps {}

export const Planning = ({ ...props }: PlanningProps) => {
  console.log("TCL: Planning -> props", props);
  const classes = useStyles();

  return (
    <PageTemplate header={<Header />}>
      <Grid container>
        <Grid item sm={12} xs={12}>
          <Card className={classes.card}>
            <Calendar />
          </Card>
        </Grid>
      </Grid>
    </PageTemplate>
  );
};
