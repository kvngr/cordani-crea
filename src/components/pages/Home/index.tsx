import React from "react";
import { Grid } from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import { PageTemplate } from "../../templates/pageTemplate";
import { Header } from "../../organisms/Header";

interface HomeProps extends RouteComponentProps {}

export const Home = ({}: HomeProps) => {
  return (
    <PageTemplate header={<Header />}>
      <Grid container>
        <Grid item xs={12}></Grid>
      </Grid>
    </PageTemplate>
  );
};
