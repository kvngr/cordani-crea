import React, { useEffect } from "react";
import { RouteComponentProps, navigate } from "@reach/router";
import { useForm } from "react-hook-form";
import { TextField, Button, Card } from "@material-ui/core";

import { isValidEmail, isValidPassword } from "../../../utils/validator";
import { errorsList } from "../../../utils/errors";
import { app } from "../../../config/app.firebase";
import styled from "styled-components";
import { useStyles } from "../../../themes/helpers/useStyle";

interface LoginProps extends RouteComponentProps<any> {}

type FormData = {
  email: string;
  password: string;
};

const LoginWrapper = styled.div`
  display: flex;
  margin: auto;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

const Form = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 600px;
  width: 600px;
`;

export const Login = ({}: LoginProps) => {
  // console.log("TCL: Login -> history", history);

  const classes = useStyles();

  const { register, unregister, errors, handleSubmit, formState } = useForm<
    FormData
  >({ mode: "onChange", validateCriteriaMode: "all" });

  useEffect(() => {
    return () => {
      unregister("email");
      unregister("password");
    };
  }, [unregister]);

  const onSubmit = handleSubmit(async ({ email, password }) => {
    try {
      await app.auth().signInWithEmailAndPassword(email, password);
      navigate("/planning");
    } catch (error) {
      console.error(error);
    }
  });

  return (
    <LoginWrapper>
      <Card className={classes.card}>
        <Form onSubmit={onSubmit}>
          <TextField
            inputRef={register({
              validate: value => isValidEmail(value) || errorsList.fields.email
            })}
            name="email"
            error={!!errors.email}
            helperText={errors.email && errors.email.message}
            id="tandard-error-helper-text"
            label="Adresse email"
            className={classes.formInputField}
          />
          <TextField
            name="password"
            inputRef={register({
              validate: value => value !== "" || errorsList.fields.password
            })}
            error={!!errors.password}
            helperText={errors.password && errors.password.message}
            id="tandard-error-helper-text"
            label="Mot de passe"
            className={classes.formInputField}
          />

          <Button
            type="submit"
            disabled={!formState.isValid}
            className={classes.formButton}
            variant="contained"
            color="primary"
          >
            Soumettre
          </Button>
        </Form>
      </Card>
    </LoginWrapper>
  );
};
