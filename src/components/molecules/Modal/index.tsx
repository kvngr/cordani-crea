import React, { ReactNode, forwardRef } from "react";
import styled from "styled-components";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import { useSpring, animated } from "react-spring";
import { useStyles } from "../../../themes/helpers/useStyle";

interface AbstractModalProps {
  label?: string;
  description?: string;
  children?: ReactNode;
  open?: any;
  onToggle: () => void;
}

interface FadeProps {
  children?: ReactNode;
  in: boolean;
  onEnter?: () => {};
  onExited?: () => {};
}

const AnimatedDiv = styled(animated.div)`
  outline: none;
  width: 100%;
  display: flex;
`;

const Fade = forwardRef<HTMLDivElement, FadeProps>(function Fade(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0 },
    to: { opacity: open ? 1 : 0 },
    onStart: () => {
      if (open && onEnter) {
        onEnter();
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    }
  });

  return (
    <AnimatedDiv ref={ref} style={style} {...other}>
      {children}
    </AnimatedDiv>
  );
});

export const AbstractModal = ({
  children,
  onToggle,
  open,
  label,
  description
}: AbstractModalProps) => {
  const classes = useStyles();

  return (
    <>
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={classes.modal}
        open={open}
        onClose={onToggle}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <div className={classes.wrapperModal}>
            <h2 id="spring-modal-title">{label}</h2>
            <p id="spring-modal-description">{description}</p>
            {children}
          </div>
        </Fade>
      </Modal>
    </>
  );
};
