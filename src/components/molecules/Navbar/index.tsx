import React from "react";
import { app } from "../../../config/app.firebase";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { useStyles } from "../../../themes/helpers/useStyle";
import { Link } from "../../atoms/Link";
import { Button } from "@material-ui/core";

interface NavbarProps {}

const links = [{ label: "Planning", to: "/planning" }];

export const Navbar = ({}: NavbarProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <li className={classes.listLinkNavbar}>
            {links.map((link, index) => (
              <li key={index}>
                <Link label={link.label} to={link.to} />
              </li>
            ))}
          </li>
          <Button variant="contained" onClick={() => app.auth().signOut()}>
            SIGN OUT
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};
