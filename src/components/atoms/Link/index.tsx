import React, { ReactNode } from "react";
import { Link as RouterLink, LinkGetProps } from "@reach/router";
import grey from "@material-ui/core/colors/grey";
import styled from "styled-components";

interface LProps {
  to: string;
  label?: string;
  replace?: boolean;
  getProps?: (props: LinkGetProps) => {};
  state?: any;
  children?: ReactNode;
  className?: string;
}

const primaryLink = grey[200];
const secondaryLink = grey[500];

const StyledLink = styled(RouterLink)`
  text-decoration: none;
  font-family: Roboto;
  padding: 10px;
`;

export const Link = ({
  to,
  replace,
  getProps,
  state,
  label,
  children,
  className,
  ...rest
}: LProps) => {
  return (
    <StyledLink
      {...rest}
      to={to}
      className={className}
      replace={replace}
      state={state}
      getProps={({ isCurrent }) => {
        return { style: { color: isCurrent ? primaryLink : secondaryLink } };
      }}
    >
      {label || children}
    </StyledLink>
  );
};
