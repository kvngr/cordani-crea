import React from "react";

interface PublicrouteProps {
  component: any;
  path: string;
  exact: boolean;
}

export const PublicRoute = ({
  component: Component,
  ...rest
}: PublicrouteProps) => <Component {...rest} />;
