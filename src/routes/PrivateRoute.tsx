import React, { useContext } from "react";
import { Redirect } from "@reach/router";
// import { AuthContext } from "../stores/auth";

interface PrivateRouteProps {
  component: any;
  path: string;
  exact: boolean;
}

export const PrivateRoute = ({
  component: Component,
  ...rest
}: PrivateRouteProps) => {
  // const { user } = useContext(AuthContext);
  const user = JSON.parse(localStorage.getItem("authUser")!);
  return user ? (
    <Component {...rest} />
  ) : (
    <Redirect from="" to="/login" noThrow />
  );
};
