export const errorsList = {
  generic: {
    unfilled: "Ce champ est requis",
    incomplete: "Il manque des champs"
  },
  fields: {
    email: "Adresse email non valide",
    password: "Mot de passe non valide"
  }
};
